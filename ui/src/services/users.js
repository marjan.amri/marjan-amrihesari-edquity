import axios from "axios";

const userServiceBaseUrl = "http://localhost:8080";

export const getUsers = async query => {
  const { data } = await axios.post(`${userServiceBaseUrl}/users`, query);
  return data;
};
