import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import UpIcon from '@material-ui/icons/KeyboardArrowUp';
import DownIcon from '@material-ui/icons/KeyboardArrowDown';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: `4px 15px`,
    minHeight: 58,
    position: 'absolute',
    bottom: '15px',
    left: 'auto',
    right: '24px',
    zIndex: 1,
  },
  paginationButtons: {
    display: 'flex',
    alignItems: 'center',
  },
  round: {
    backgroundColor: '#aaa',
    '&:first-of-type': {
      marginRight: 10,
    },
  },
}));

const Controls = ({ onPageUp, onPageDown, disabled }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.paginationButtons}>
        <IconButton onClick={onPageDown} className={classes.round}>
          <DownIcon />
        </IconButton>
        <IconButton onClick={onPageUp} className={classes.round} disabled={disabled}>
          <UpIcon />
        </IconButton>
      </div>
    </div>
  );
};

export default Controls;
