import React, { useState, useEffect, useRef } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import TableFooter from '@material-ui/core/TableFooter';
import Controls from './Controls';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
  root: {
    flexShrink: 0,
    marginLeft: 15,
  },
});


export default function CustomizedTables({ data, loadMore }) {
  const classes = useStyles();
  const headerRef = useRef();
  const [skip, setSkip] = useState(0);
  const [richedTheLimit, setRichedTheLimit] = useState(false);

  const headers = data.reduce((output, entry) => {
    const result = output;
    Object.keys(entry).map((key) => {
      if (!result.includes(key)) result.push(key);
    });
    return result;
  }, []);

  const processLoadMore = async () => {
    try {
      const result = await loadMore(skip);
      console.log(result)
      setRichedTheLimit(result?.length === 0);
    } catch (error) {
      console.error(error);
      setRichedTheLimit(true);
    }
  };
  
  const page = direction => () => {
    setSkip(skip + (direction === 'down'? 1: -1) * data.length);
  };

  useEffect(() => {
    if(skip < 0) return;
    processLoadMore();
  }, [skip]);

  return (
    <TableContainer component={Paper} >
      <Table className={classes.table} aria-label="customized table">
        <TableHead ref={headerRef}>
          <TableRow>
            {headers.map((key) => {
              return (
                <StyledTableCell>
                  {
                    // Convert camelcased values to uppercased values to be used as
                    // dynamic headers
                    key
                      .replace(/([A-Z])/g, " $1")
                      .replace(/^./, function (str) {
                        return str.toUpperCase();
                      })
                  }
                </StyledTableCell>
              );
            })}
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row) => (
            <StyledTableRow key={row.name}>
              {headers.map((key) => {
                return <StyledTableCell>{row[key]}</StyledTableCell>;
              })}
            </StyledTableRow>
          ))}
        </TableBody>
        <TableFooter>
          <Controls
            onPageUp={page('up')}
            onPageDown={page('down')}
            disabled={!skip}
          />
        </TableFooter>
      </Table>
    </TableContainer>
  );
}
