import React, { useState, useEffect } from "react";
import "./App.css";
import Table from "./components/table.jsx";
import { Container, Button, Typography } from "@material-ui/core";
import formatCurrency from "./utils/formatCurrency";

import { getUsers } from "./services/users.js";
import { getApplications, createApplication } from "./services/applications.js";
import { getPayments, createPayment } from "./services/payments.js";

const App = () => {
  /**
   * Hydrate data for the table and set state for users, applications, and payments
   */
  const [users, setUsers] = useState([]);
  const [applications, setApplications] = useState([]);
  const [payments, setPayments] = useState([]);
  const [dataLoaded, setDataLoaded] = useState(false);
  const [error, setError] = useState();

  useEffect(() => {
    async function fetchData() {
      const [usersData, applicationsData, paymentsData] = await Promise.all([
        getUsers(),
        getApplications(),
        getPayments(),
      ]);

      setUsers(usersData.body);
      setApplications(applicationsData.body);
      setPayments(paymentsData.body);
      setDataLoaded(true);
    }
    fetchData();
  }, []);

  useEffect(() => {
    if(error)
      setTimeout(() => {
        setError('');
      }, 5000);
  }, [error])

  const initiatePayment = async ({ applicationUuid, requestedAmount }) => {
    if (!applicationUuid) {
      setError('No application id have been found! please contact manager');
      return;
    }
    if (!requestedAmount) {
       setError('No payment is needed');
       return;
    }
    const { body } = await createPayment({
      applicationUuid,
      requestedAmount,
    });
    setPayments([...payments, body]);
  };

  const initiateApplication = async ({ userUuid, requestedAmount}) => {
    if (!userUuid) {
      setError('No user Id have been found! please contact manager');
      return;
    }
    const { body } = await createApplication({
      userUuid,
      requestedAmount,
    });
    console.log('body =>', body)
    setApplications([...applications, body]);
  }

  const loadMore = async skip => {
    const userData = await getUsers({ skip });
    setUsers(userData.body);
    return userData.body;
  }

  let tableData = [];
  if (dataLoaded) {
    tableData = users.map(({ uuid, name, email }) => {
      const { requestedAmount, uuid: applicationUuid } =
      applications.find((application) => application.userUuid === uuid) || {};
      const { paymentAmount, paymentMethod } =
      payments.find(
        (payment) => payment.applicationUuid === applicationUuid
      ) || {};

      // Format table data to be passed into the table component, pay button tacked
      // onto the end to allow payments to be issued for each row
      return {
        uuid,
        name,
        email,
        requestedAmount: formatCurrency(requestedAmount),
        paymentAmount: formatCurrency(paymentAmount),
        paymentMethod,
        initiatePayment: (
          <>
            {!paymentAmount && applicationUuid && (
              <Button
                onClick={() =>
                  initiatePayment({
                    applicationUuid,
                    requestedAmount,
                  })
                }
                variant="contained"
              >
                Pay
              </Button>
            )}
            {!applicationUuid && (
              <Button
                onClick={() =>
                  initiateApplication({
                    userUuid: uuid,
                    requestedAmount,
                  })
                }
                variant="contained"
              >
                Application
              </Button>
            )}
          </>
        ),
      };
    });
  }

  return (
    <div className="App">
      {error && <Typography color="error">{error}</Typography>}
      <Container>{dataLoaded && <Table data={tableData} loadMore={loadMore}/>}</Container>
    </div>
  );
};

export default App;
